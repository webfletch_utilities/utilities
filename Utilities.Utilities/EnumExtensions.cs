﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public partial class Ext
    {
        public static string EnumValueToString<T>(this T enumValue) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T must be an enumerated type");
            
            return typeof(T).EnumList().Single(x => x.Key == (int)(IConvertible)enumValue).Value;
        }

        public static List<KeyValuePair<int, string>> EnumList(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new ArgumentException("T must be an enumerated type");
            
            return Enum.GetValues(enumType).Cast<object>().Select(e => new KeyValuePair<int, string>((int)e, GetDescription(e).Replace("'", "''"))).ToList();
        }

        private static string GetDescription(object enumValue)
            => GetAttribute(enumValue) ?? SplitCamelCase(enumValue.ToString());

        private static string GetAttribute(object enumValue)
            => enumValue?.GetType()
                ?.GetMember(enumValue.ToString())
                ?.First()
                ?.GetCustomAttribute<System.ComponentModel.DataAnnotations.DisplayAttribute>()
                ?.Name;

        private static string SplitCamelCase(string str)
            => Regex.Replace(Regex.Replace(str, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1 $2"), @"(\p{Ll})(\P{Ll})", "$1 $2");
    }
}
