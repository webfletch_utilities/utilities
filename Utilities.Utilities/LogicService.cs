﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JinnDev.Utilities.Utilities
{
    public class LogicService
    {
        public static bool CollectionFulfillsLogic(List<string> inputCollection, string logicExpression, bool ands = false)
        {
            if (string.IsNullOrWhiteSpace(logicExpression) || logicExpression.Trim() == "*") return true;

            if (string.IsNullOrWhiteSpace(logicExpression)) return true;
            logicExpression = logicExpression.Trim(',', ' ');

            if (logicExpression.Length == PeelSquares(logicExpression).Length)
                return CollectionFulfillsLogic(inputCollection, logicExpression.Substring(1, logicExpression.Length - 2), logicExpression.StartsWith("["));

            if (!logicExpression.Contains(","))
            {
                if (logicExpression.StartsWith("!"))
                {
                    if (inputCollection.Any(x => x.ToLower() == logicExpression.Trim('!').ToLower())) return false;
                    return true;
                }
                else
                {
                    if (inputCollection.Any(x => x.ToLower() == logicExpression.ToLower())) return true;
                    return false;
                }
            }
            else
            {
                var commaSeparated = new List<string>();
                var currentExpression = "";
                while (logicExpression.Length > 0)
                {
                    var chr = logicExpression[0];
                    if (chr == ',')
                    {
                        commaSeparated.Add(currentExpression);
                        currentExpression = "";
                        logicExpression = logicExpression.Substring(1);
                        continue;
                    }

                    if (chr == '[' || chr == '<')
                    {
                        if (currentExpression.Length > 0) throw new Exception("Can't peel with currentExpression");
                        var peeled = PeelSquares(logicExpression);
                        logicExpression = logicExpression.Substring(peeled.Length).Trim(',', ' ');
                        commaSeparated.Add(peeled);
                        continue;
                    }

                    currentExpression += chr;
                    logicExpression = logicExpression.Substring(1);
                }

                if (!string.IsNullOrWhiteSpace(currentExpression)) commaSeparated.Add(currentExpression);

                if (ands == false) return commaSeparated.Any(x => CollectionFulfillsLogic(inputCollection, x, ands));
                else return commaSeparated.All(x => CollectionFulfillsLogic(inputCollection, x, ands));
            }
        }

        private static string PeelSquares(string logicExpression)
        {
            if (!(logicExpression.StartsWith("[") || logicExpression.StartsWith("<"))) return "";
            var depth = 0;
            var currentExpression = "";
            foreach (var chr in logicExpression)
            {
                currentExpression += chr;

                if (chr == '[' || chr == '<') depth++;

                if (chr == ']' || chr == '>')
                {
                    depth--;
                    if (depth == 0) break;
                }

            }

            return currentExpression;
        }
    }
}