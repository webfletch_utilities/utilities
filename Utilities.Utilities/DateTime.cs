﻿using System;
namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        public static DateTime GetFirstDayOfMonth(this DateTime today)
        {
            DateTime endOfMonth = new DateTime(today.Year, today.Month, 1);
            return endOfMonth;
        }

        public static DateTime GetLastDayOfMonth(this DateTime today)
        {
            DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
            return endOfMonth;
        }
        
        public static DateTime GetLastMillisecondOfMonth(this DateTime today)
        {
            DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month), 23, 59, 59);
            return endOfMonth;
        }

        public static bool Between(this DateTime currDate, DateTime startInclusive, DateTime endExclusive)
        {
            return (currDate >= startInclusive && currDate < endExclusive);
        }

        public static DateTime ToDate(this string value)
        {
            if (double.TryParse(value, out double val))
                return DateTime.FromOADate(val);
            return DateTime.Parse(value);
        }
    }
}