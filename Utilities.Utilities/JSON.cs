﻿using Newtonsoft.Json;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static string Serialize<T>(this T obj)
        {
            if (typeof(T).IsValueType) throw new System.Exception("You can only serialize Reference Types");
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public static T Deserialize<T>(this string convertFrom)
            => JsonConvert.DeserializeObject<T>(convertFrom);
    }
}