﻿using JinnDev.Utilities.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static bool IsNumeric(this string value) => float.TryParse(value, out _);

        public static bool IsAlpha(this string value, bool allowUnderscore = false)
            => allowUnderscore ? value.Replace("_", "").All(x => char.IsLetter(x)) : value.All(x => char.IsLetter(x));
        
        public static bool IsAlphaNumeric(this string value, bool allowUnderscore = false, bool allowPeriod = false, bool allowComma = false)
        {
            if (allowUnderscore) value = value.Replace("_", "");
            if (allowPeriod) value = value.Replace(".", "");
            if (allowComma) value = value.Replace(",", "");
            return value.All(x => char.IsLetterOrDigit(x));
        }

        public static bool IsChars(this string value, CharTypes includeTypes = CharTypes.AlphaNumeric)
        {
            var matchRGX = "";
            if (includeTypes == CharTypes.None) return string.IsNullOrEmpty(value);
            if (includeTypes.HasFlag(CharTypes.MathSymbols)) matchRGX += @"-\p{Sm}\*\/\+x"; // MUST be first, so the minus/dash is the first thing in the character group.
            if (includeTypes.HasFlag(CharTypes.LowercaseAlpha)) matchRGX += @"\p{Ll}";
            if (includeTypes.HasFlag(CharTypes.UpercaseAlpha)) matchRGX += @"\p{Lu}";
            if (includeTypes.HasFlag(CharTypes.Alpha)) matchRGX += @"\p{L}\p{M}";
            if (includeTypes.HasFlag(CharTypes.Spaces)) matchRGX += @"\p{Z}";
            if (includeTypes.HasFlag(CharTypes.Numeric)) matchRGX += @"\d";
            if (includeTypes.HasFlag(CharTypes.NumericPunctuated)) matchRGX += @"\d.,\p{Sc}";
            if (includeTypes.HasFlag(CharTypes.Quotes)) matchRGX += @"\p{Pi}\p{Pf}""'";
            if (includeTypes.HasFlag(CharTypes.Punctuation)) matchRGX += @"\p{Po}";
            if (includeTypes.HasFlag(CharTypes.OtherPunctuation)) matchRGX += @"\p{So}\p{Pd}\p{Pc}";
            if (includeTypes.HasFlag(CharTypes.Parenthesis)) matchRGX += @"\(\)";
            if (includeTypes.HasFlag(CharTypes.SquareBrackets)) matchRGX += @"\[\]";
            if (includeTypes.HasFlag(CharTypes.SharpBrackets)) matchRGX += @"\<\>";
            if (includeTypes.HasFlag(CharTypes.CurlyBraces)) matchRGX += @"\{\}";
            if (includeTypes.HasFlag(CharTypes.Brackets)) matchRGX += @"\p{Ps}\p{Pe}\(\)\[\]\<\>\{\}";

            var rgx = string.Format("[^{0}-[{1}]]", matchRGX, @"\p{C}\p{Sk}");
            return !Regex.IsMatch(value, rgx);
        }

        public static bool IsTruthy(this string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return false;
            if (value == "0") return false;
            if (value.ToLower() == "null") return false;
            if (value.ToLower() == "false") return false;
            if (IsNullCharacters(value)) return false;
            return true;
        }

        public static bool IsNullCharacters(this string value)
            => (value.Contains('\0') && value.Trim('\0').Length == 0);

        public static bool IsBasicEmail(this string value) => Regex.IsMatch(value, @"[^@]+@[^@]+\.[^@]+");

        public static bool IsBasicPhoneNumber(this string value) => Regex.Replace(value, @"\D", "").Length > 6 && Regex.Replace(value, @"\D", "").Length < 16;

        public static string QuickReplace(this string input, string ifThis, string thenReplaceThisPattern, string withThis)
            => input.ContainsI(ifThis) ? Regex.Replace(input, thenReplaceThisPattern, withThis) : input.Trim();

        public static bool ContainsI(this string input, string value)
            => input.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;

        public static int IndexOfI(this string input, string value)
            => input.IndexOf(value, StringComparison.OrdinalIgnoreCase);

        public static bool EqualsAnyI(this string text, params string[] options)
            => options.Any(x => text.EqualsI(x));

        public static bool StartsWithAnyI(this string text, params string[] options)
            => options.Any(x => text.StartsWithI(x));

        public static bool EndsWithAnyI(this string text, params string[] options)
            => options.Any(x => text.EndsWithI(x));

        public static List<int> IndexesOf(this string text, string splitChar)
        {
            var indexes = new List<int>();
            var i = -1;
            while ((i = text.IndexOf(splitChar, i < 0 ? 0 : i + splitChar.Length)) >= 0)
                indexes.Add(i);
            return indexes;
        }

        public static List<Tuple<int, string>> IndexesAndMatchOfRegex(this string text, string pattern)
        {
            var indexes = new List<Tuple<int, string>>();
            Match match = null;
            var rgx = new Regex(pattern);
            while ((match = rgx.Match(text, match == null ? 0 : match.Index + match.Value.Length)).Success)
                indexes.Add(new Tuple<int, string>(match.Index, match.Value));
            return indexes;
        }

        public static List<Tuple<string, string>> RegexSplitDetail(this string text, string pattern)
        {
            var splitAreas = new List<Tuple<string, string>>();

            var regexResult = Regex.Matches(text, pattern);
            if (regexResult.Cast<Match>().Select(x => x.Groups.Count).Any(x => x != 1)) throw new Exception("A Split Regex Pattern should not contain ANY Capture Groups.  Instead, use Non-Capturing Groups: (?:xxx)");
            var regexSplit = Regex.Split(text, pattern);

            for (var i = 0; i < regexSplit.Length; i++)
                splitAreas.Add(new Tuple<string, string>(i == 0 ? null : regexResult[i - 1].Value, regexSplit[i]));

            return splitAreas;
        }

        public static bool StartsWithI(this string input, string value)
            => input.StartsWith(value, StringComparison.OrdinalIgnoreCase);

        public static bool EndsWithI(this string input, string value)
            => input.EndsWith(value, StringComparison.OrdinalIgnoreCase);

        public static bool EqualsI(this string input, string value)
            => input.Equals(value, StringComparison.OrdinalIgnoreCase);

        public static int Count(this string input, char value)
            => input.Length - input.Replace(value.ToString(), string.Empty).Length;

        public static string ReplaceFirstI(this string text, string search, string replace, int startIndex = 0)
        {
            var pos = text.IndexOf(search, startIndex, StringComparison.OrdinalIgnoreCase);
            return pos < 0 ? text : text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        public static bool ContainsAny(this string text, params char[] options)
            => options.Any(x => text.Count(x) > 0);

        public static bool ContainsAnyI(this string text, params string[] options)
            => options.Any(x => text.IndexOfI(x) > 0);

        public static string StartsWithWhichI(this string text, params string[] options)
                => options.SingleOrDefault(x => text.StartsWithI(x));
    }
}