﻿using JinnDev.Utilities.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static string BOX { get => "allBoxedUp"; }

        public static int BoxLen { get => BOX.Length + 3; }
        
        public static void AddOrUpdate<T>(this Dictionary<string, T> dct, string key, T value)
        {
            if (dct.ContainsKey(key)) dct[key] = value;
            else dct.Add(key, value);
        }

        public static void AddOrAppend<T>(this Dictionary<string, List<T>> dct, string key, T value)
        {
            if (dct.ContainsKey(key)) dct[key].Add(value);
            else dct.Add(key, new List<T> { value });
        }

        public static void AddOnce<T>(this Dictionary<string, T> dct, string key, T value)
        {
            if (!dct.ContainsKey(key)) dct.Add(key, value);
        }

        public static void AddOnce<T>(this List<T> dct, T value)
        {
            if (!dct.Contains(value)) dct.Add(value);
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
            => enumerable == null ? true : (enumerable as ICollection<T>) != null ? (enumerable as ICollection<T>).Count < 1 : !enumerable.Any();

        public static U Get<T, U>(this Dictionary<T, U> dict, T key) where U : class
        {
            U val = default(U);
            var success = dict.TryGetValue(key, out val);
            if (success) return val;

            return null;
        }

        public static string Peek(this BoxedString box, string stringToPeek)
        {
            stringToPeek = stringToPeek.Replace("cstr999", "c_str()");
            if (string.IsNullOrWhiteSpace(stringToPeek))
            {
                return string.Empty;
            }

            var boxNumIndexes = new Dictionary<int, string>();
            var boxNameIndexes = stringToPeek.IndexesOf(BOX);
            foreach (var boxNameIndex in boxNameIndexes)
            {
                var boxNumIndex = stringToPeek.Substring(boxNameIndex + BOX.Length, 3);
                var boxNum = int.Parse(boxNumIndex);
                boxNumIndexes.Add(boxNum, boxNumIndex);
            }

            foreach (var boxNumIndex in boxNumIndexes)
            {
                stringToPeek = stringToPeek.Replace(BOX + boxNumIndex.Value, box.Boxes[boxNumIndex.Key]);
            }

            return stringToPeek;
        }

        private static void MakeGroupsSafe2(char openChar, char closeChar, ref string text, ref List<string> boxes)
        {
            if (!text.Contains(openChar))
            {
                return;
            }

            if (text.IndexOf("ÔÔ") >= 0)
            {
                throw new Exception("Can't use ÔÔ");
            }

            text = text.Replace("->", "ÔÔ");
            var safeString = text;
            var lastIndex = 0;
            while (true)
            {
                var oIndex = safeString.IndexOf(openChar, lastIndex);
                if (oIndex < 0)
                {
                    break;
                }

                var cIndex = safeString.IndexOf(closeChar, oIndex);
                if (cIndex < 0)
                {
                    break;
                }

                var tmpSafeString = safeString.Substring(oIndex + 1, cIndex - oIndex - 1);
                if (openChar == '<' && tmpSafeString.ContainsAnyI("&&", "||"))
                {
                    lastIndex = oIndex + 1;
                    continue;
                }

                var rgxParen = new Regex(RegexHelpers.RgxDynamic(openChar, closeChar) + @"(?<!\" + openChar + BOX + @"\d+\" + closeChar + @")");
                var parenArea = rgxParen.Match(safeString, lastIndex);
                var parenText = parenArea.Groups[0].ToString();
                if (string.IsNullOrWhiteSpace(parenText))
                {
                    break;
                }

                var parenLength = 1;
                if (!(openChar == '<' && parenText.ContainsAnyI("&&", "||")) && !string.IsNullOrWhiteSpace(parenText.Trim(openChar, closeChar)))
                {
                    var textText = text.Substring(parenArea.Index, parenArea.Length);
                    var replacement = openChar + BOX + boxes.Count.ToString().PadLeft(3, '0') + closeChar;
                    boxes.Add(textText.Substring(1, textText.Length - 2).Replace("ÔÔ", "->"));
                    safeString = safeString.ReplaceFirstI(parenText, replacement);
                    text = text.ReplaceFirstI(textText, replacement);
                    parenLength = replacement.Length;
                }

                lastIndex = parenArea.Index + parenLength;
            }

            text = text.Replace("ÔÔ", "->");
        }
    }
}