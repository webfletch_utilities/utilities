﻿namespace JinnDev.Utilities.Monad
{
    public class Maybe<T> : MaybeBase
    {
        public T Value { get; set; }

        public Maybe()
        {
            Value = default(T);
        }
        public Maybe(T value)
        {
            Value = value;
            HasValue = true;
        }
        public Maybe(T value, string message)
        {
            Value = value;
            Message = message;
            HasValue = true;
        }
    }
}