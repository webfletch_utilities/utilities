﻿using System;

namespace JinnDev.Utilities.Monad
{
    public abstract class MaybeBase
    {
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public bool IsExceptionState { get => Exception != null; }
        public bool HasValue { get; set; }
    }
}