﻿using System.Collections.Generic;

namespace JinnDev.Utilities.Utilities.Models
{
    public class BoxedString
    {
        public string OriginalString { get; set; }
        public string CleanedString { get; set; }
        public List<string> Boxes { get; set; }
    }
}