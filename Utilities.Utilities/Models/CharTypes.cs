﻿using System;
using System.ComponentModel;

namespace JinnDev.Utilities.Utilities.Models
{
    [Flags]
    public enum CharTypes
    {
        [Description("None: Matches if a string has no characters.  Null, string.empty, or ''")]
        None = 0,

        [Description("LowercaseAlpha : Matches if a string has letters which have both Upper and Lower variants, but specifically the lower")]
        LowercaseAlpha = 1, //       \p{Lowercase_Letter}

        [Description("UpercaseAlpha : Matches if a string has letters which have both Upper and Lower variants, but specifically the upper")]
        UpercaseAlpha = 2, //        \p{Uppercase_Letter}

        [Description("Alpha: Matches all letters of all languages")]
        Alpha = 4, //                \p{L}\p{M}

        [Description("Spaces: Matches separators of all sorts, spacebar, tab, vertical, paragraph, etc.")]
        Spaces = 8, //               \p{Separator}

        [Description("Numeric: Matches purely numeric, such as 1234")]
        Numeric = 16, //             \d

        [Description("NumericPunctuated: Matches formatted numeric, such as $1,234.00 or $$$12,,,5.")]
        NumericPunctuated = 32, //   \d.,\p{Currency_Symbol}

        [Description("Quotes: Matches single, double, open, and closing quotes")]
        Quotes = 64, //              \p{Initial_Punctuation}\p{Final_Punctuation}

        [Description("Punctuation: Matches all typical punctuation")]
        Punctuation = 128, //        \p{Other_Punctuation}

        [Description("OtherPunctuation: Matches 'Other' punctuation, like underscore, dash, etc.")]
        OtherPunctuation = 256, //   \p{Other_Symbol}\p{Dash_Punctuation}\p{Connector_Punctuation}

        [Description("MathSymbols: Matches mathematical symbols and operators")]
        MathSymbols = 512, //        \p{Math_Symbol}

        [Description("Parenthesis: Matches purely parenthesis ()")]
        Parenthesis = 1024, //       \(\)

        [Description("SquareBrackets: Matches purely square brackets []")]
        SquareBrackets = 2048, //    \[\]

        [Description("SharpBrackets: Matches purely angle brackets <>")]
        SharpBrackets = 4096, //     <>

        [Description("CurlyBraces: Matches purely curly braces {}")]
        CurlyBraces = 8192, //       \{\}

        [Description("Brackets: Matches all braces")]
        Brackets = 16384, //         \p{Open_Punctuation}\p{Close_Punctuation}
                          // -[\p{Other}\p{Modifier_Symbol}]

        [Description("AlphaNumeric: Matches Alpha and Numeric")]
        AlphaNumeric = Alpha | Numeric,

        [Description("Password: Matches everything except Brackets and Quotes")]
        Password = Alpha | Spaces | Numeric | Punctuation | OtherPunctuation | MathSymbols,

        [Description("All: O.O")]
        All = Password | Brackets | Quotes,
    }
}