﻿using JinnDev.Utilities.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        /// <summary>
        /// Find the Index of a substring, but not counting instances within Quoted Areas.
        /// </summary>
        public static int UnquotedIndexOf(this string input, string value, int startIndex = 0)
        {
            var tmpString = input.MakeQuotedAreasSafe();
            return tmpString.IndexOf(value, startIndex);
        }

        /// <summary>
        /// Replaces text between quoted areas with Underscores
        /// </summary>
        public static string MakeQuotedAreasSafe(this string text)
        {
            int startIndex = 0;
            var replacements = new List<string>();
            if (!string.IsNullOrWhiteSpace(text) && (text.Contains(@"""") || text.Contains("'")))
            {
                while (true)
                {
                    if (startIndex > text.Length) break;

                    var quotedArea = Regex.Match(text.Substring(startIndex), RegexHelpers.AllQuotes);
                    var fixedArea = quotedArea.Groups[0].ToString();

                    if (string.IsNullOrWhiteSpace(fixedArea)) break;

                    var quoteType = fixedArea[0];

                    if (fixedArea.Length > 2 && !fixedArea.ContainsI(BOX))
                        text = text.ReplaceFirstI(quotedArea.Groups[0].ToString(), quoteType + string.Empty.PadLeft(fixedArea.Length - 2, '_') + quoteType, startIndex);

                    startIndex = quotedArea.Index + startIndex + fixedArea.Length;
                }
            }

            return text;
        }

        public static string Unbox(this string text, List<string> boxes)
        {
            text = text.Replace("cstr999", "c_str()");
            if (boxes == null || boxes.Count == 0 || text.IndexOf(BOX) < 0)
            {
                return text;
            }

            var whileCount = 0;
            while (true)
            {
                whileCount++;
                if (whileCount > 400)
                {
                    throw new Exception("wtf");
                }

                text = text.Replace("cstr999", "c_str()");
                var boxIndex = text.IndexOf(BOX);
                if (boxIndex < 0)
                {
                    break;
                }

                var strBoxNum = text.Substring(boxIndex + BOX.Length, 3);
                var boxNum = int.Parse(strBoxNum);
                text = text.Replace(BOX + strBoxNum, boxes[boxNum]);
            }

            return text;
        }

        public static BoxedString PackBox(this string text, List<string> currentBoxes = null)
        {
            if (currentBoxes == null)
            {
                currentBoxes = new List<string>();
            }

            var originalText = text;
            var boxes = new List<string>();
            if (currentBoxes.Count > 0)
            {
                boxes.AddRange(currentBoxes);
            }

            int quoteBoxCount = 0;
            if (currentBoxes.Count == 0)
            {
                text = text.Replace(".c_str()", ".cstr999");
                var quoteSafe = text.SaveQuotedAreas(boxes);
                text = quoteSafe.Item1;
                boxes = quoteSafe.Item2;
                quoteBoxCount = quoteSafe.Item2.Count;
            }

            MakeGroupsSafe2('{', '}', ref text, ref boxes);
            MakeGroupsSafe2('(', ')', ref text, ref boxes);
            MakeGroupsSafe2('[', ']', ref text, ref boxes);
            MakeGroupsSafe2('<', '>', ref text, ref boxes);

            if (currentBoxes.Count > 0)
            {
                var updateIndex = boxes.IndexOf(originalText);
                if (updateIndex < 0)
                {
                    throw new Exception("wtf");
                }

                boxes[updateIndex] = text;
            }

            var packedTighter = new List<string>();
            packedTighter.AddRange(boxes);
            foreach (var item in boxes.Skip(quoteBoxCount + currentBoxes.Count))
            {
                var box = item.PackBox(packedTighter);
                packedTighter = box.Boxes;
            }

            boxes = packedTighter;

            return new BoxedString { OriginalString = originalText, CleanedString = text, Boxes = boxes };
        }

        private static Tuple<string, List<string>> SaveQuotedAreas(this string text, List<string> currentBoxes)
        {
            int startIndex = 0;
            var replacements = new List<string>();
            if (currentBoxes != null && currentBoxes.Count > 0)
            {
                replacements.AddRange(currentBoxes);
            }

            if (!string.IsNullOrWhiteSpace(text) && (text.Contains(@"""") || text.Contains("'")))
            {
                while (true)
                {
                    if (startIndex > text.Length)
                    {
                        break;
                    }

                    var quotedArea = Regex.Match(text.Substring(startIndex), RegexHelpers.AllQuotes);
                    var fixedArea = quotedArea.Groups[0].ToString();
                    if (string.IsNullOrWhiteSpace(fixedArea))
                    {
                        break;
                    }

                    var fixedAreaLen = fixedArea.Length;
                    var quoteType = fixedArea[0];
                    if (fixedAreaLen > 2 && !fixedArea.ContainsI(BOX) && !string.IsNullOrWhiteSpace(fixedArea.Trim(quoteType)))
                    {
                        text = text.ReplaceFirstI(quotedArea.Groups[0].ToString(), quoteType + BOX + replacements.Count.ToString().PadLeft(3, '0') + quoteType, startIndex);
                        replacements.Add(fixedArea.Substring(1, fixedAreaLen - 2));
                        fixedAreaLen = BoxLen + 2;
                    }

                    startIndex = quotedArea.Index + startIndex + fixedAreaLen;
                }
            }

            return new Tuple<string, List<string>>(text, replacements);
        }
    }
}
