﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;

namespace JinnDev.Utilities.Utilities
{
    public class JinnAsync : IDisposable
    {
        int _outstandingOperations;
        JinnSynchronizationContext _innerContext;
        BlockingEnumerator _blocker;
        TaskSchedulerBase _taskScheduler;
        public TaskFactory _taskFactory;

        public JinnAsync()
        {
            _blocker = new BlockingEnumerator();
            _innerContext = new JinnSynchronizationContext(this);
            _taskScheduler = new TaskSchedulerBase(this);
            _taskFactory = new TaskFactory(CancellationToken.None, TaskCreationOptions.HideScheduler, TaskContinuationOptions.HideScheduler, _taskScheduler);
        }

        public static JinnAsync Current { get => (SynchronizationContext.Current as JinnSynchronizationContext)?.Context; }

        public void Execute()
        {
            var oldContext = SynchronizationContext.Current;
            try
            {
                SynchronizationContext.SetSynchronizationContext(_innerContext);
                foreach (var task in _blocker.SelectTask())
                {
                    _taskScheduler.ExecuteTask(task.Item1);
                    if (task.Item2) task.Item1.GetAwaiter().GetResult();
                }
            }
            finally
            {
                SynchronizationContext.SetSynchronizationContext(oldContext);
            }
        }

        public void Dispose() => _blocker.Dispose();

        public void Begin() => Interlocked.Increment(ref _outstandingOperations);

        public void End()
        {
            if (Interlocked.Decrement(ref _outstandingOperations) == 0)
                _blocker.CompleteAdding();
        }

        public void AddQueue(Task task, bool prepegateExceptions)
        {
            Begin();
            task.ContinueWith(_ => End(), CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, _taskScheduler);
            _blocker.Add(task, prepegateExceptions);
        }

        public static TResult Await<TResult>(Func<Task<TResult>> action)
        {
            using (var context = new JinnAsync())
            {
                context.Begin();
                var task = context._taskFactory.StartNew(action).Unwrap().ContinueWith(t =>
                {
                    context.End();
                    return t.GetAwaiter().GetResult();
                }, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, context._taskScheduler);
                context.Execute();
                return task.GetAwaiter().GetResult();
            }
        }

        private class BlockingEnumerator : IDisposable
        {
            BlockingCollection<Tuple<Task, bool>> _blocker = new BlockingCollection<Tuple<Task, bool>>();

            public IEnumerable<Tuple<Task, bool>> SelectTask() => _blocker.GetConsumingEnumerable();

            public IEnumerable<Task> SelectTasks() => _blocker.Select(x => x.Item1);

            public void Add(Task task, bool prepegateExceptions)
            {
                try { _blocker.TryAdd(Tuple.Create(task, prepegateExceptions)); }
                catch { }
            }

            public void CompleteAdding() => _blocker.CompleteAdding();

            public void Dispose() => _blocker.Dispose();
        }

        private class TaskSchedulerBase : TaskScheduler
        {
            public override int MaximumConcurrencyLevel { get => 1; }
            JinnAsync _outerContext;

            public TaskSchedulerBase(JinnAsync context) => _outerContext = context;

            public void ExecuteTask(Task task) => TryExecuteTask(task);

            protected override IEnumerable<Task> GetScheduledTasks() => _outerContext._blocker.SelectTasks();
            protected override void QueueTask(Task task) => _outerContext.AddQueue(task, false);
            protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued) =>
                (SynchronizationContext.Current == _outerContext._innerContext) && TryExecuteTask(task);
        }
    }

    public class JinnSynchronizationContext : SynchronizationContext
    {
        public JinnAsync Context { get; private set; }

        public JinnSynchronizationContext(JinnAsync context)
        {
            Context = context;
        }

        public override void Post(SendOrPostCallback d, object state)
            => Context.AddQueue(Context._taskFactory.StartNew(() => d(state)), true);

        public override void Send(SendOrPostCallback d, object state)
        {
            if (JinnAsync.Current == Context) d(state);
            else Context._taskFactory.StartNew(() => d(state)).GetAwaiter().GetResult();
        }

        public override void OperationStarted() => Context.Begin();
        public override void OperationCompleted() => Context.End();
        public override SynchronizationContext CreateCopy() => new JinnSynchronizationContext(Context);
        public override int GetHashCode() => Context.GetHashCode();
        public override bool Equals(object obj) => (obj as JinnSynchronizationContext) == null ? false : (obj as JinnSynchronizationContext).Context == Context;
    }
}