﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace JinnDev.Utilities.Utilities
{
    public static partial class Ext
    {
        public static string MemberMessage(this Exception ex, Dictionary<string, string> parms = null, [System.Runtime.CompilerServices.CallerMemberName] string methodName = null)
        {
            var methodSignature = string.Format("{0}({1})", methodName, string.Join(", ", parms?.Select(x => string.Format("{0}:{1}", x.Key, x.Value)) ?? new List<string>()));
            return "Exception From " + methodSignature + "\r\n" + _recursiveMessage(ex);
        }
        public static string RecursiveMessage(this Exception ex) => _recursiveMessage(ex);

        private static string _recursiveMessage(Exception ex, string text = "", int depth = 0)
        {
            if (ex == null) return text.Trim();
            var thisText = ex.Message + "\r\nStack Trace: " + ex.StackTrace;
            thisText = Regex.Replace(thisText, @"(?m)^\s*", Tab(depth));
            text += "\r\n\r\n" + thisText;
            return _recursiveMessage(ex.InnerException, text, depth + 1);
        }

        private static string Tab(int depth) => "".PadLeft(depth * 4, ' ');
    }
}