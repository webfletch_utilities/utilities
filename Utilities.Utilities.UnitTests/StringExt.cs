﻿using JinnDev.Utilities.Utilities;
using JinnDev.Utilities.Utilities.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Utilities.Utilities.UnitTests
{
    [TestClass]
    public class StringExt
    {
        [TestMethod]
        public void IsChars_NormalStuff()
        {
            string nullString = null;
            Assert.AreEqual(true, nullString.IsChars(CharTypes.None));
            Assert.AreEqual(true, "".IsChars(CharTypes.None));
            Assert.AreEqual(true, string.Empty.IsChars(CharTypes.None));
            Assert.AreEqual(false, " ".IsChars(CharTypes.None));
            Assert.AreEqual(true, "asdf".IsChars(CharTypes.LowercaseAlpha));
            Assert.AreEqual(false, "aBsdf".IsChars(CharTypes.LowercaseAlpha));
            Assert.AreEqual(true, "ASDF".IsChars(CharTypes.UpercaseAlpha));
            Assert.AreEqual(false, "AbSDF".IsChars(CharTypes.UpercaseAlpha));
            Assert.AreEqual(true, "aSÔ".IsChars(CharTypes.Alpha));
            Assert.AreEqual(true, "\r\n \t".IsChars(CharTypes.Spaces));
            Assert.AreEqual(false, "\r\n \tXXX".IsChars(CharTypes.Spaces));
            Assert.AreEqual(true, "1234".IsChars(CharTypes.Numeric));
            Assert.AreEqual(false, "two".IsChars(CharTypes.Numeric));
            Assert.AreEqual(false, "12.34".IsChars(CharTypes.Numeric));
            Assert.AreEqual(true, "$1,332.24".IsChars(CharTypes.NumericPunctuated));
            Assert.AreEqual(true, @"'""'“”".IsChars(CharTypes.Quotes));
            Assert.AreEqual(false, @"Quote".IsChars(CharTypes.Quotes));
            Assert.AreEqual(true, "!!?".IsChars(CharTypes.Punctuation));
            Assert.AreEqual(true, "_-".IsChars(CharTypes.OtherPunctuation));
            Assert.AreEqual(false, "_-!".IsChars(CharTypes.OtherPunctuation));
            Assert.AreEqual(true, "*/+-x".IsChars(CharTypes.MathSymbols));
            Assert.AreEqual(false, "*\\+-".IsChars(CharTypes.MathSymbols));
            Assert.AreEqual(true, ")(".IsChars(CharTypes.Parenthesis));
            Assert.AreEqual(true, "][".IsChars(CharTypes.SquareBrackets));
            Assert.AreEqual(true, "><".IsChars(CharTypes.SharpBrackets));
            Assert.AreEqual(true, "}{".IsChars(CharTypes.CurlyBraces));
            Assert.AreEqual(true, ")(}{][><".IsChars(CharTypes.Brackets));

            Assert.AreEqual(true, "1a2bC4D".IsChars(CharTypes.AlphaNumeric));
            Assert.AreEqual(true, "aA\t12\v\r\n!!_?-*/".IsChars(CharTypes.Password));
            Assert.AreEqual(false, "aA\t12\v\r\n!!_?-*/'[".IsChars(CharTypes.Password));
            Assert.AreEqual(true, "aA\t12\v\r\n!!_?-*/'[".IsChars(CharTypes.All));
        }
    }
}