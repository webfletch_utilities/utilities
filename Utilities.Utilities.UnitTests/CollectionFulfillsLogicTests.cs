﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JinnDev.Utilities.Utilities;

namespace Utilities.Utilities.UnitTests
{
    [TestClass]
    public class CollectionFulfillsLogicTests
    {
        [TestMethod]
        public void MessyInputCollectionsWork()
        {
            Assert.AreEqual(true, UserCanAccess("1", "1"));
            Assert.AreEqual(true, UserCanAccess("1,2,3,4 ", "1"));
            Assert.AreEqual(true, UserCanAccess("4,2,1,  3", "1"));
            Assert.AreEqual(true, UserCanAccess("1, 2,1, ", "1"));
            Assert.AreEqual(true, UserCanAccess("1,", "1"));

            Assert.AreEqual(false, UserCanAccess("21", "1"));
            Assert.AreEqual(false, UserCanAccess("15,2,3,4 ", "1"));
            Assert.AreEqual(false, UserCanAccess("4,2,15,  31", "1"));
            Assert.AreEqual(false, UserCanAccess("51, 2,15, ", "1"));
            Assert.AreEqual(false, UserCanAccess("15,", "1"));
        }

        [TestMethod]
        public void UserHasOne_ExpressionAllowsOne()
        {
            Assert.AreEqual(true, UserCanAccess("1", "1"));
        }

        [TestMethod]
        public void UserHasTwo_ExpressionAllowsOne()
        {
            Assert.AreEqual(true, UserCanAccess("1,2", "1"));
            Assert.AreEqual(true, UserCanAccess("1,2", "2"));
        }

        [TestMethod]
        public void UserHasSome_ExpressionDeniesOne()
        {
            Assert.AreEqual(false, UserCanAccess("1,2", "!1"));
            Assert.AreEqual(false, UserCanAccess("1,2", "!2"));
        }

        [TestMethod]
        public void InclusiveOrExclusive()
        {
            var expression = "1,!5";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,4,6", expression));
            Assert.AreEqual(true, UserCanAccess("1,5,8", expression));
            Assert.AreEqual(true, UserCanAccess("2,3,4", expression));
            Assert.AreEqual(false, UserCanAccess("2,3,4,5", expression));
        }

        [TestMethod]
        public void Exclusives()
        {
            var expression = "!1,!5";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression)); // Doesn't have a 5... pass
            Assert.AreEqual(true, UserCanAccess("1,4,6", expression)); // Doesn't have a 5... pass
            Assert.AreEqual(false, UserCanAccess("1,5,8", expression));
            Assert.AreEqual(true, UserCanAccess("2,3,4", expression)); // Doesn't have a 1... pass
            Assert.AreEqual(true, UserCanAccess("2,3,4,5", expression)); // Doesn't have a 1... pass
        }

        [TestMethod]
        public void MultipleInclusiveOneExclusive()
        {
            var expression = "1,3,!5,8";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("4,6,8", expression));
            Assert.AreEqual(true, UserCanAccess("1,5,8", expression));
            Assert.AreEqual(false, UserCanAccess("2,4,5,7", expression));
        }

        [TestMethod]
        public void InclusiveAnds()
        {
            var expression = "[1,4]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4,8", expression));
            Assert.AreEqual(false, UserCanAccess("1,5,8", expression));
            Assert.AreEqual(false, UserCanAccess("2,4,7", expression));
        }

        [TestMethod]
        public void InclusiveAndExclusive()
        {
            var expression = "[3,!5]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4,8", expression));
            Assert.AreEqual(false, UserCanAccess("1,5,8", expression));
            Assert.AreEqual(false, UserCanAccess("2,4,7", expression));
        }

        [TestMethod]
        public void ExclusiveAnds()
        {
            var expression = "[!5,!7]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4,8", expression));
            Assert.AreEqual(false, UserCanAccess("1,5,8", expression));
            Assert.AreEqual(false, UserCanAccess("2,4,7", expression));
        }

        [TestMethod]
        public void InclusiveAndsWithInclusiveOrs()
        {
            var expression = "[1,2],3";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4", expression));
            Assert.AreEqual(false, UserCanAccess("1,4,5", expression));
        }

        [TestMethod]
        public void InclusiveAndsWithInclusiveOrsBackward()
        {
            var expression = "1,[2,3]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4", expression));
            Assert.AreEqual(false, UserCanAccess("2,4,5", expression));
        }

        [TestMethod]
        public void InclusiveAndsWithExclusiveOrs()
        {
            var expression = "[1,2],!3";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(false, UserCanAccess("1,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,4,5", expression));
        }

        [TestMethod]
        public void MessyAndsWithInclusiveOrs()
        {
            var expression = "1,[!2,3]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(false, UserCanAccess("2,3,4,5", expression));
        }

        [TestMethod]
        public void DoubleDeep()
        {
            var expression = "[1,2,<3,4>]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,2,3", expression));
            Assert.AreEqual(true, UserCanAccess("1,2,4", expression));
            Assert.AreEqual(false, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(false, UserCanAccess("1,2,5", expression));
            Assert.AreEqual(false, UserCanAccess("1,3", expression));
        }

        [TestMethod]
        public void DoubleDeepNegativeAnds()
        {
            var expression = "[1,!2,<3,4>]";
            Assert.AreEqual(true, UserCanAccess("1,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,3", expression));
            Assert.AreEqual(true, UserCanAccess("1,4", expression));
            Assert.AreEqual(false, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(false, UserCanAccess("1,2,4", expression));
            Assert.AreEqual(false, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(false, UserCanAccess("1,2,5", expression));
        }

        [TestMethod]
        public void DoubleDeepNegativeOrs()
        {
            var expression = "[1,2,<3,!4>]";
            Assert.AreEqual(true, UserCanAccess("1,2,3,4", expression));
            Assert.AreEqual(true, UserCanAccess("1,2,3", expression));
            Assert.AreEqual(true, UserCanAccess("1,2", expression));
            Assert.AreEqual(false, UserCanAccess("1,2,4", expression));
            Assert.AreEqual(false, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(false, UserCanAccess("1,3", expression));
        }

        [TestMethod]
        public void Inception()
        {
            var expression = "1,2,[3,4,<5,6,[7,8],9>]";
            Assert.AreEqual(true, UserCanAccess("1", expression));
            Assert.AreEqual(true, UserCanAccess("2", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,6", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,7,8", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,9", expression));

            Assert.AreEqual(false, UserCanAccess("9", expression));
            Assert.AreEqual(false, UserCanAccess("3", expression));
            Assert.AreEqual(false, UserCanAccess("3,4", expression));
            Assert.AreEqual(false, UserCanAccess("3,4,7", expression));
            Assert.AreEqual(false, UserCanAccess("3,4,8", expression));
        }

        [TestMethod]
        public void NegativeInception()
        {
            var expression = "!1,2,[3,4,<5,!6,[!7,8],!9>]";
            Assert.AreEqual(true, UserCanAccess("", expression));
            Assert.AreEqual(true, UserCanAccess("3", expression)); // 3 == !1
            Assert.AreEqual(true, UserCanAccess("2", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(true, UserCanAccess("3,4", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,8", expression));

            Assert.AreEqual(false, UserCanAccess("1,3", expression));
            Assert.AreEqual(false, UserCanAccess("1,3,4,6,9", expression));
        }

        [TestMethod]
        public void NoPermissionRequired()
        {
            var expression = "";
            Assert.AreEqual(true, UserCanAccess(null, expression));
            Assert.AreEqual(true, UserCanAccess("", expression));
            Assert.AreEqual(true, UserCanAccess("3", expression)); // 3 == !1
            Assert.AreEqual(true, UserCanAccess("2", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(true, UserCanAccess("3,4", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,8", expression));

            Assert.AreEqual(true, UserCanAccess("1,3", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4,6,9", expression));
        }

        [TestMethod]
        public void AnyPermission()
        {
            var expression = "*";
            Assert.AreEqual(true, UserCanAccess(null, expression));
            Assert.AreEqual(true, UserCanAccess("", expression));
            Assert.AreEqual(true, UserCanAccess("3", expression)); // 3 == !1
            Assert.AreEqual(true, UserCanAccess("2", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,5", expression));
            Assert.AreEqual(true, UserCanAccess("3,4", expression));
            Assert.AreEqual(true, UserCanAccess("3,4,8", expression));

            Assert.AreEqual(true, UserCanAccess("1,3", expression));
            Assert.AreEqual(true, UserCanAccess("1,3,4,6,9", expression));
        }

        private bool UserCanAccess(string userPerms, string logicExpression)
        {
            var inputCollections = userPerms?.Split(',').ToList();
            return LogicService.CollectionFulfillsLogic(inputCollections, logicExpression);
        }
    }
}